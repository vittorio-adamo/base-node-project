const host = window.location.host
const protocol = window.location.protocol
const path = window.location.pathname.split('/').filter(function (n) {
  return n !== ''
})
const env = {
  hostUrl: protocol + '//' + host,
  path: path
}

module.exports = env

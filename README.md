# base-node-project

------------

## Package configuration

**- Project Prerequisites**

1. [NVM](https://github.com/creationix/nvm)
  [6233d853]: https://github.com/creationix/nvm "nvm"

  **- Execute**

  ```
  - nvm install
  - cp .env.sample .env
  - npm install
  ```
  **- Start**
  ```
  npm run start:dev // run dev server
npm run test:watch // run test
  ```
  ------------

## AWS CodeDeploy configuration

**- Valid AMI**
```
- nodejs-6.5-pm2-nvm-json-awslogs (ami-50750923)
```

**- Naming Convenction** _AWS codedeploy scripts prerequisite_

```
- APPLICATION_NAME: base-node-project-[ test, staging, prod]
- IAM role: base-node-project-[ test, staging, prod]
- EXTERNAL LOAD BALANCER: base-node-project-[ test, staging, prod]-elb
- INTERNAL LOAD BALANCER: base-node-project-[ test, staging, prod]-ilb
- DEPLOYMENT_GROUP_NAME: base-node-project-[ test, staging, prod]
```

**- Prerequisites**
```
1. Create s3 buckets with the right .env file

  - s3://test-{APPLICATION_NAME}-satispay-com/env/.env
  - s3://staging-{APPLICATION_NAME}-satispay-com/env/.env
  - s3://{APPLICATION_NAME}-satispay-com/env/.env
```

**- Edit scripts/bundle**

  ```
APPLICATION_NAME={APPLICATION_NAME}
  ```


**- Edit appspec.yml**

```
files:
  - source: /
    destination: /home/satispay/{APPLICATION_NAME}
```

**- Edit scripts/aws/codedeploy/AfterInstall.sh**

```
sudo chown -R satispay /home/satispay/{APPLICATION_NAME}
```

**- Edit scripts/aws/codedeploy/common_functions.sh**
_uncomment the right option for the load-balancer_

```
# ELB_LIST="${LB} ${LBI}" # using internal and external
# ELB_LIST="${LBI}" # using only the internal
# ELB_LIST="${LB}" # using only the external

```

**- AWS**
```
1. create vpc/subnet
2. create security-group
3. create IAM role (Common-CodeDeploy+CloudWatch + s3 policy for download the package+ satispay-log)
4. launch ec2/autoscalingGroup from ami
5. codedeploy Service role CodeDeploy
```

/* eslint-env jest */
const app = require('../app')
const request = require('supertest')

describe('Ensure server is up', () => {
  it('GET /', function (done) {
    request(app)
      .get('/')
      .expect(200)
      .end(done)
  })
})

var express = require('express')
var router = express.Router()
const logger = require('satispay-node-logger')(__filename)

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  logger.info(`Time: ${Date.now()}`)
  next()
})

router.get('/', function (req, res) {
  res.render('index', {
    title: 'Hey Hey Hey!',
    message: 'Yo Yo'
  })
})

// define the about route
router.get('/about', function (req, res) {
  res.render('index', {
    title: 'Hey Hey Hey!',
    message: 'About Birds'
  })
})

module.exports = router

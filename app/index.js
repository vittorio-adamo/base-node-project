/* eslint-env node */
require('dotenv').config()

// 3rd part modules
const express = require('express')
const compression = require('compression')
const cors = require('cors')
const boom = require('express-boom')
const bodyParser = require('body-parser')

// Node.js core modules
const path = require('path')

// App Constant
const isProduction = process.env.NODE_ENV === 'production'
const PORT = process.env.PORT || 6000

// Custom Dependencies
const logger = require('satispay-node-logger')(__filename)
const middlewares = require('satispay-node-connect-middlewares')

const app = express()

// Good Stuff
app.disable('x-powered-by')
app.use(compression())

// Format Error
app.use(boom())

// Enable Cors in production
if (!isProduction) {
  app.use(cors())
}

// satispay-node-connect-middlewares
app.use(middlewares.cid())

// Log à la Morgan with our middleware
app.use(middlewares.logRequests(logger))

// Basic parser configuration
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// Static files
app.use(express.static(path.join(__dirname, '..', 'public')))

// View Engine
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// Routes
const router = require('./routes')
app.use(router)

// Middleware
require('./middleware')(app)

// Cluster
const cluster = require('cluster')
const numCPUs = require('os').cpus().length

if (process.env.NODE_ENV !== 'dev' && process.env.NODE_ENV !== 'DEBUG') {
  if (cluster.isMaster) {
    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
      cluster.fork()
      logger.info(`worker ${i} it's up`)
    }

    cluster.on('exit', (worker, code, signal) => {
      logger.info(`worker ${worker.process.pid} died`)
    })
  } else {
    if (!module.parent) { app.listen(PORT) }
    logger.info(`base-node-project listening on port ${PORT}`)
  }
} else {
  if (!module.parent) { app.listen(PORT) }
  logger.info(`base-node-project listening on port ${PORT}`)
}

module.exports = app

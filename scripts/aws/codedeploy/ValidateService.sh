#!/bin/env sh

while [ true ]; do
  HTTP_STATUS_CODE=$(curl --silent --output /dev/null --write-out "%{http_code}" localhost:5000)

  if [[ "$HTTP_STATUS_CODE" == "200" ]]; then
    break
  else
    echo "Not responding. Retry in 2 seconds"
  fi

  sleep 2
done

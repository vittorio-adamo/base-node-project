#!/bin/env sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

if [ "$DEPLOYMENT_GROUP_NAME" == "${APPLICATION_NAME}-test" ]
then
    aws s3 cp "s3://test-${APPLICATION_NAME}-satispay-com/env/.env" "/home/satispay/${APPLICATION_NAME}" || {
      echo "cannot download environment file from s3://test-${APPLICATION_NAME}-satispay-com/env/.env"
      exit 1
    }
elif [ "$DEPLOYMENT_GROUP_NAME" == "${APPLICATION_NAME}-staging" ]
then
    aws s3 cp "s3://staging-${APPLICATION_NAME}-satispay-com/env/.env" "/home/satispay/${APPLICATION_NAME}" || {
      echo "cannot download environment file from s3://staging-${APPLICATION_NAME}-satispay-com/env/.env"
      exit 1
    }
elif [ "$DEPLOYMENT_GROUP_NAME" == "${APPLICATION_NAME}-prod" ]
then
    aws s3 cp "s3://${APPLICATION_NAME}-satispay-com/env/.env" "/home/satispay/${APPLICATION_NAME}" || {
      echo "cannot download environment file from s3://${APPLICATION_NAME}-satispay-com/env/.env"
      exit 1
    }
fi

cd /home/satispay/${APPLICATION_NAME}
mkdir /home/satispay/log/
pm2 --name ${APPLICATION_NAME} -l /home/satispay/log/out.log -f start app

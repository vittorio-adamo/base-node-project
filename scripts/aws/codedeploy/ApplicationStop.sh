#!/bin/env sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

STATUS_CMD="pm2 jlist | json -c 'this.name === \"${APPLICATION_NAME}\"' 0.pm2_env.status"
STATUS=$(eval $STATUS_CMD)


if [[ "$STATUS" == "online" ]]; then
  echo "Stopping and deleting pm2 ${APPLICATION_NAME} app"
  pm2 delete "${APPLICATION_NAME}"
else
  echo "${APPLICATION_NAME} not running. Nothing to do"
fi

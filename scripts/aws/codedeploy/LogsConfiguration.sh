#!/bin/env sh

ENVIRONMENT=$(sed -r "s/${APPLICATION_NAME}-//g" <<< "${DEPLOYMENT_GROUP_NAME}")
#get environment from deployment group_name (codedeploy) base-node-project-test
CONF_PATH=/etc/awslogs/awslogs.conf

cat >${CONF_PATH} <<EOL
[general]
state_file = /var/lib/awslogs/agent-state
use_gzip_http_content_encoding = true

[${DEPLOYMENT_GROUP_NAME}]
datetime_format = %Y-%m-%dT%H:%M:%S.%fZ
file = /home/satispay/log/out-0.log
log_stream_name = {instance_id}-${APPLICATION_NAME}
buffer_duration = 5000
initial_position = end_of_file
log_group_name = ${ENVIRONMENT}
EOL

service awslogs restart

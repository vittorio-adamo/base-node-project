#!/bin/sh
set -e

APPLICATION_NAME="base-node-project"
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
NVM_DEFAULT_PATH="$HOME/.nvm/nvm.sh"
BUNDLE_NAME="${APPLICATION_NAME}-$(git describe --tags)"
BUNDLE_FILENAME="$BUNDLE_NAME.tar.gz"
START_TIME=$(date +%s)

echo "-----> load nvm"
if [[ ! -f $NVM_DEFAULT_PATH ]]; then
  echo >&2 '`nvm` is required';
  echo >&2 'see https://github.com/creationix/nvm';
  exit 1;
else
  set +e;
  . $NVM_DEFAULT_PATH;
  nvm use
  set -e # disable error handling
fi

echo "-----> rebuild node-sass"
npm rebuild node-sass

echo "-----> installing dependencies"
npm install

echo "-----> clean compiled assets"
npm run clean

echo "-----> build"
NODE_ENV=production npm run build

echo "-----> uglify"
npm run uglify

echo "-----> backup node_modules directory"
cp -R node_modules node_modules_dev

echo "-----> use only production dependencies (it saves spaces)"
npm prune --production

echo "-----> package"
echo > "$BUNDLE_FILENAME" # clear any previous tarball with the same name
tar czvf "$BUNDLE_FILENAME" \
  app \
  public/scripts.min.js \
  public/style.min.css \
  node_modules \
  package.json \
  README.md \
  appspec.yml \
  scripts/aws/codedeploy

echo "-----> kindly restore previous node_modules"
rm -r node_modules
mv node_modules_dev node_modules

echo "-----> the end"
END_TIME=$(date +%s)
DIFF=$(( $END_TIME - $START_TIME ))
echo "\n==================="
echo "Package completed."
echo "It tooks $DIFF seconds and produced '$BUNDLE_FILENAME'."
echo "Use 'aws s3 cp $BUNDLE_FILENAME s3://release-satispay/${APPLICATION_NAME}/${BUNDLE_FILENAME}' to copy into release bucket"
